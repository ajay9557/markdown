# 5 NoSQL database with high performance

**NoSQL**

- A NoSQL referring to non SQL or non-relational is a database that provides a mechanism for storage and retrieval of data.
  It provides different data model such as-

  - Document
  - Key-Value
  - Wide-Column
  - Graph

  **Features**

  1. Multi-Model
  2. Easily Scalable
  3. Flexible
  4. Distributed

**Why use NoSQL Database**

- NoSQL is non-relational database. It's basically used to handle large amounts of traffic and give better performance. It has the capacity to store large amounts of data and handle it in a flexible way so some big companies like Google, Yahoo, Facebook use it. NoSQL is good for the companies who work with large data and the data structure they manage are variable.

# 5 NoSQL Databases

1. MongoDB
2. Cassandra
3. Redis
4. HBase
5. Neo4j

## Why should we use them\*\*

**1. MongoDB**

- It is a document data model type database built on a horizontal scale-out architecture. Each row in this database is a document described in JSON format.
- Fields in a document play the role of columns in a SQL database, and like columns, they can be indexed to increase search performance.
  Here’s a simple JSON document describing contact information:

```
{
  "name" :  "Carlos Smith",
  "title" : "Product Manager",
  "location" : "New York, NY",
  "twitter" : "@MongoDB",
  "facebook" : "@MongoDB"
}
```

- Many developers and companies are using MongoDB because the document data model allows them to store and retrieve data fast.
- it can handle both large amounts of data and traffic.

  **Use Case**

  - Customer analytics
  - Product data management
  - Content management
  - Mobile development
  - Real-time data integration

**2. Cassandra**

- Apache Cassandra is an open-source and distributed storage system, for managing very large amounts of structured data. It provides a highly available service with no single point of failure.
- It uses a column-oriented data model.
- Cassandra is being used by some of the biggest companies such as Facebook, Twitter, Cisco, Rackspace, eBay, Twitter, Netflix, and more.
- Cassandra consists of all possible data formats including structured, semi-structured, and unstructured. It can dynamically accommodate changes to your data structures according to your need.
- Can Handle Massive Datasets.
- Having a Homogeneous Environment that means Cassandra does not require outside support for synchronization. All of the required components for basic operation are built directly into Cassandra
- providing Highly Fault Tolerant
- having Amazing Community

  **Use Case**

  - Messaging
  - Handle high-speed Applications
  - Product Catalogs and retail apps
  - Social Media Analytics

**3. Redis**

- Redis, which stands for Remote Dictionary Server, is a fast, open-source database.
- According to Redis homepage-
  "Redis is an open-source (BSD licensed), in-memory data structure store, used as a database, cache, and message broker. "
- Redis avoid seek time delays and can access data in microseconds.
- Redis supports many data structures-
  - Strings
  - Lists
  - Sets
  - Sorted
  - Hashes
  - Bitmaps
  - HyperLogLogs
- In-memory store: Basically means that All data in Redis is stored in RAM, delivering the fastest possible access times to the data for both read and write requests.
- Optimized for speed: Being written in ANSI C, it compiles into extremely efficient machine code and requires little overhead.
- Support for arbitrary data: It can store any data from human-readable text to encoded binaries.
- Languages like JavaScript, Java, Go, C, C++, C#, Python, Objective-C, PHP and almost everyone supports this.
- Highly available

  **Use Cases**

  - Session Cache
  - Queues
  - Usage & Metered Billing

**4. HBase**

- Apache HBase is an open-source, distributed, versioned, non-relational database, built on top of Hadoop and HDFS (Hadoop Distributed File system).
- It comprises a set of standard tables with rows and columns, much like a traditional database. Each table must have an element defined as a primary key, and all access attempts to HBase tables must use this primary key.
- HBase applications are written in Java.
- Distributed storage: HBase supports distributed storage like HDFS
- Consistency: It supports consistent read and writes operations
- API support: HBase supports Java APIs so clients can access it easily
- MapReduce support: HBase supports MapReduce for parallel processing of large volume of data
- Real-time processing: It supports block cache and Bloom filters. So, real-time query processing is easy

  **Use Case**

  - Use of HBase by Mozilla: They generally store all crash data in HBase
  - Use of HBase by Facebook: Facebook uses HBase storage to store real-time messages.

**5. Neo4j**

- Neo4j is the open-source, Graph model type Database which is developed using Java technology. It is highly scalable and schema-free.
- Neo4j is referred to as a native graph database because it implements the property graph model down to the storage level.
- Neo4j provides its own implementation of graph theory concepts-
  - Nodes
  - Relationships
  - Labels
  - Properties
- Highly Performant Read and Write Scalability, Without Compromise
- Easier than Ever to Load Your Data into Neo4j-Choose how much and which data to import, without worrying about volume
- Biggest and Most Active Graph Community
- In relational databases, performance suffers as the number and depth of relationships increases. In graph databases like Neo4j, performance remains high even if the amount of data grows significantly.

  **Use Case**

  - Neo4j is used today by thousands of companies and organizations in almost all industries, including financial services, government, energy, technology, retail, and manufacturing.
  - Fraud detection and analytics.
  - Network and database infrastructure monitoring.

**References**

- [MongoDB](https://www.mongodb.com/why-use-mongodb)
- [Cassandra](https://dzone.com/articles/the-top-10-reasons-to-use-cassandra-database)
- [Redis](https://aws.amazon.com/redis/)
- [Hbase](https://blog.eduonix.com/bigdata-and-hadoop/use-hbase-nosql-db/)
- [Neo4j](https://rubygarage.org/blog/neo4j-database-guide-with-use-cases)
